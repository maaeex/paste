<?php
namespace Maex\Paste\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Maex.Paste".            *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ActionController;
use Maex\Paste\Domain\Model\Paste;

class PasteController extends ActionController {

	/**
	 * @Flow\Inject
	 * @var \Maex\Paste\Domain\Repository\PasteRepository
	 */
	protected $pasteRepository;

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('pastes', $this->pasteRepository->findAll());
	}

	/**
	 * @param \Maex\Paste\Domain\Model\Paste $paste
	 * @return void
	 */
	public function showAction(Paste $paste) {
		$this->view->assign('paste', $paste);
	}

	/**
	 * @return void
	 */
	public function newAction() {
	}

	/**
	 * @param \Maex\Paste\Domain\Model\Paste $newPaste
	 * @return void
	 */
	public function createAction(Paste $newPaste) {
		$this->pasteRepository->add($newPaste);
		$this->addFlashMessage('Created a new paste.');
		$this->redirect('index');
	}

	/**
	 * @param \Maex\Paste\Domain\Model\Paste $paste
	 * @return void
	 */
	public function editAction(Paste $paste) {
		$this->view->assign('paste', $paste);
	}

	/**
	 * @param \Maex\Paste\Domain\Model\Paste $paste
	 * @return void
	 */
	public function updateAction(Paste $paste) {
		$this->pasteRepository->update($paste);
		$this->addFlashMessage('Updated the paste.');
		$this->redirect('index');
	}

	/**
	 * @param \Maex\Paste\Domain\Model\Paste $paste
	 * @return void
	 */
	public function deleteAction(Paste $paste) {
		$this->pasteRepository->remove($paste);
		$this->addFlashMessage('Deleted a paste.');
		$this->redirect('index');
	}

}

?>