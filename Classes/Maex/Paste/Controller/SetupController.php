<?php
namespace Maex\Paste\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Maex.Paste".            *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;

class SetupController extends \TYPO3\Flow\Mvc\Controller\ActionController {

  /**
   * @Flow\Inject
   * @var \Maex\Paste\Domain\Repository\ServiceRepository
   */
  protected $serviceRepository;

  /**
   * @Flow\Inject
   * @var \Maex\Paste\Domain\Repository\PasteRepository
   */
  protected $pasteRepository;

  /**
   * Sets up a fresh service and creates a sample paste.
   *
   * @return void
   */
  public function indexAction() {
    $this->serviceRepository->removeAll();
    $this->pasteRepository->removeAll();

    $service = new \Maex\Paste\Domain\Model\Service();
    $service->setTitle('My Paste Service');
    $service->setDescription('Feel free to paste');
    $this->serviceRepository->add($service);

    $paste = new \Maex\Paste\Domain\Model\Paste();
    $paste->setTitle('Example Paste');
    $paste->setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');
    $this->pasteRepository->add($paste);
    var_dump($paste);
    $service->addPaste($paste);

    return 'Successfully created a paste service';
  }
}

?>
