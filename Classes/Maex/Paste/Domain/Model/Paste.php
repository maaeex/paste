<?php
namespace Maex\Paste\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Maex.Paste".            *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Paste {

	/**
	 * The blog
	 * @var \Maex\Paste\Domain\Model\Service
	 * @ORM\ManyToOne(inversedBy="pastes")
	 */
	protected $service;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * The content
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $content;

	/**
	 * @var \DateTime
	 */
	protected $date;

	/**
	 * Constructs this paste
	 */
	public function __construct() {
    $this->date = new \DateTime();
	}

	/**
	 * @return \Maex\Paste\Domain\Model\Service
	 */
	public function getService() {
		return $this->service;
	}

	/**
	 * @param \Maex\Paste\Domain\Model\Service $service
	 * @return void
	 */
	public function setService(\Maex\Paste\Domain\Model\Service $service) {
		$this->service = $service;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * @param string $content
	 * @return void
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * @return \DateTime
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @param \DateTime $date
	 * @return void
	 */
	public function setDate($date) {
		$this->date = $date;
	}

}
?>
