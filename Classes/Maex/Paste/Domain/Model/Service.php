<?php
namespace Maex\Paste\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Maex.Paste".            *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Service {

	/**
	 * The services's title.
	 *
	 * @var string
	 * @Flow\Validate(type="Text")
	 * @Flow\Validate(type="StringLength", options={ "minimum"=1, "maximum"=80 })
	 * @ORM\Column(length=80)
	 */
	protected $title = '';

	/**
	 * A short description of the service
	 *
	 * @var string
	 * @Flow\Validate(type="Text")
	 * @Flow\Validate(type="StringLength", options={ "maximum"=150 })
	 * @ORM\Column(length=150)
	 */
	protected $description = '';

	/**
   * The pastes contained in this service
   *
   * @var \Doctrine\Common\Collections\Collection<\Maex\Paste\Domain\Model\Paste>
   * @ORM\OneToMany(mappedBy="service")
   * @ORM\OrderBy({"date" = "DESC"})
   */
  protected $pastes;

	/**
   * Constructs a new Service
   */
  public function __construct() {
  	$this->pastes = new \Doctrine\Common\Collections\ArrayCollection();
  }

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getPastes() {
		return $this->pastes;
	}

	/**
   * Adds a paste to this service
   *
   * @param \Maex\Paste\Domain\Model\Paste $paste
   * @return void
   */
  public function addPaste(\Maex\Paste\Domain\Model\Paste $paste) {
    $paste->setPaste($this);
    $this->pastes->add($paste);
  }

	/**
   * Removes a paste from this service
   *
   * @param \Maex\Paste\Domain\Model\Paste $paste
   * @return void
   */
  public function removePaste(\Maex\Paste\Domain\Model\Paste $paste) {
          $this->pastes->removeElement($past);
  }

}
?>
