<?php
namespace Maex\Paste\Domain\Repository;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Maex.Paste".            *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class PasteRepository extends Repository {

	// add customized methods here

}
?>