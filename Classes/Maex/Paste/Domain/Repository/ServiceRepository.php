<?php
namespace Maex\Paste\Domain\Repository;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Maex.Paste".            *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class ServiceRepository extends Repository {

  /**
   * Finds pastes by the specified service
   *
   * @param \Maex\Paste\Domain\Model\Service $service The service the paste must refer to
   * @param integer $limit The number of pastes to return at max
   * @return \TYPO3\Flow\Persistence\QueryResultInterface The pastes
   */
  public function findByService(\Maex\Paste\Domain\Model\Service $pastes) {
    $query = $this->createQuery();
    return $query->matching($query->equals('blog', $blog))
      ->setOrderings(array('date' => QueryInterface::ORDER_DESCENDING))
      ->execute();
  }

}
?>
